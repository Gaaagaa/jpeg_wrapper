﻿/**
 * @file main.cpp
 * Copyright (c) 2024 Gaaagaa. All rights reserved.
 * 
 * @author  : Gaaagaa
 * @date    : 2024-10-02
 * @version : 1.0.0.0
 * @brief   : 显示 JPEG 图片基本信息 程序。
 */

#include "jwrapper.h"

////////////////////////////////////////////////////////////////////////////////

/**********************************************************/
/**
 * @brief 输出程序帮助信息。
 */
void usage(const char * xsz_name)
{
    printf("usage: %s jpeg_file\n", xsz_name);
}

/**********************************************************/
/**
 * @brief main function.
 */
int main(int argc, char * argv[])
{
    j_int_t     jit_err  = JDEC_ERR_UNKNOWN;
    j_cstring_t jsz_file = J_NULL;
    jdecoder_t  jdecoder;
    jpeg_info_t jpeginfo;

    //======================================

    if (argc < 2)
    {
        usage(argv[0]);
        return -1;
    }

    jsz_file = argv[1];

    //======================================
    // 配置图像输入源

    jit_err = jdecoder.config(JCTL_MODE_FSZPATH, (j_fhandle_t)jsz_file, 0);
    if (JDEC_ERR_OK != jit_err)
    {
        printf(
            "jdecoder.config(, [%s],) return error: %s\n",
            jsz_file,
            jdec_errno_name(jit_err));
        return -1;
    }

    //======================================
    // 获取图像基本信息

    jit_err = jdecoder.info(&jpeginfo);
    if (JDEC_ERR_OK != jit_err)
    {
        printf(
            "jdecoder.config(, [%s],) return error: %s\n",
            jsz_file,
            jdec_errno_name(jit_err));
        return -1;
    }

    printf(
        "jpeg image %s\n"
        "   size W x H : %d x %d\n"
        "   channels   : %d\n"
        "   colorspace : %s\n",
        jsz_file,
        jpeginfo.jit_imgw,
        jpeginfo.jit_imgh,
        jpeginfo.jit_nchs,
        jpeg_cs_name(jpeginfo.jcs_type));

    //======================================

    return jit_err;
}

////////////////////////////////////////////////////////////////////////////////
